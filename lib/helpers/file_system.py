"""

This is a helper module for the graspPak Package Manager Manager it contains several functions that we can use to easily
(and quickly) manipulate the file system (eg. creating directories) as well as gather information about surrounding
files

"""

import inspect

from os.path import isdir
from os import listdir

print(inspect.stack()[-1])
