#!/usr/bin/env python

"""



"""

from lib.helpers.logger import start as start_log
from lib.helpers.text_pallet import Color, Format, Effects

from termcolor import colored

from configparser import ConfigParser
from argparse import ArgumentParser

c = Color()
f = Format()
fx = Effects()

parent_parser = ArgumentParser(add_help=False)

parent_parser.add_argument('--verbose',
                           dest='verbose',
                           action='store_true',
                           required=False,
                           help=f'Get detailed output from the program. This is excessive and serves no purpose'
                                f' to most users. This usually would be used by developers, an individual troubleshooting'
                                f' unexpected behavior, or the overly curious. ({c.blue}Note: Could also be used to make you'
                                f' feel like a {c.light_red}1337 hax0r{f.end_mod})')

main_parser = ArgumentParser(prog='graspPak',
                             prefix_chars='+-',
                             description='',
                             add_help=True,
                             allow_abbrev=True)

subcommands = main_parser.add_subparsers(dest='sub')

sub_generate_defaults = subcommands.add_parser('generate-defaults',
                                               help="Generate a config file that contains optimized defaults for your "
                                                    "environment.",
                                               parents=[parent_parser
                                                        ])

sub_clean = subcommands.add_parser('clean',
                                   help='Remove all config files/temp files. You\'ll need to run "configure" again if'
                                        'you want to use graspPak')

args = main_parser.parse_args()

log = start_log('graspPak:configure', args.verbose)
info = log.info
info('')

# Set up a variable to hold our configuration
config = ConfigParser()
