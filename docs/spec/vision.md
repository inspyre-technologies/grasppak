# graspPak General Write-Up
## Abstract

The current vision for graspPak as an  application is a rather simple one. graspPak will work as an extensible Package Manager for Linux (developed in a Kubuntu 18.04 Environment) that can search, update, and install packages from multiple package managers. Adding support for your favorite package managers would be as simple as creating for finding a graspPak Extension

## What Does it Offer?

 - The ability to search all your favorite package managers at once with a single short command for a package
 - Define a preferred package manager and see the results you want to see first
 - Install your desired package directly from the search results!
 - More to come!

## Terminology

 - **Package Manager**:
    A **package manager** or **package-management system** is a collection of software tools that automates the process of installing, upgrading, configuring, and removing computer programs for a [computer](https://en.wikipedia.org/wiki/Computer "Computer")'s [operating system]    (https://en.wikipedia.org/wiki/Operating_system "Operating system") in a consistent manner.

 - **Extension**:
    One could also accurately describe an extension as a plug-in (or even module) for the graspPak application. I chose extension as a matter of preference because I see these packages as *extending* graspPak's *primary* functionality, not just adding a small feature.

    At this time the extensions that I plan to have graspPak shipped with are:

   - **grasp-apt**: A graspPak Core Extension, for managing apt (*Enabled by default for debian based distros*)
   - **grasp-snap:** A graspPak Core Extension, for managing snapd packages (*Enabled by default for Ubuntu based distros*)
   - **grasp-pip**: A graspPak Core Extension for managing pip(3) packages (*Not enabled by default*)
   - **grasp-gems**: A graspPak Core Extension for managing Ruby Gems (*Not enabled by default*)

    Other extensions that I'm considering being shipped with graspPak:
    
    - **grasp-npm**
